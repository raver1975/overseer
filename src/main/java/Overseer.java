import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacv.CanvasFrame;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.FrameGrabber;

import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_core.cvAnd;
import static org.bytedeco.javacpp.opencv_imgproc.*;

/**
 * Created by Paul on 4/17/2016.
 */
public class Overseer {

    public Overseer() {

        FFmpegFrameGrabber grabber = new FFmpegFrameGrabber("video000240.mp4");
        try {
            grabber.start();
        } catch (FrameGrabber.Exception e) {
            e.printStackTrace();
        }
        IplImage image1 = null;
        IplImage image2 = null;
        IplImage image3 = null;
        IplImage diff1 = null;
        IplImage diff2 = null;
        IplImage comdiff = null;
        IplImage scan=null;
        IplImage out=null;

        CanvasFrame cf = new CanvasFrame("Overseer", 1d);

        int lengthInFrames = grabber.getLengthInFrames();
        for (int i = 0; i < lengthInFrames; i++) {
            System.out.println("frame: " + i);
            try {
                image1 = Utilities.conv.convertToIplImage(grabber.grab());
            } catch (FrameGrabber.Exception e) {
                e.printStackTrace();
            }


            if (image1 != null && image2 != null && image3 != null) {
                diff1 = Utilities.diff(image1, image2);
                diff2 = Utilities.diff(image2, image3);

//                IplConvKernel kernelx = IplConvKernel.create(3, 3, 1, 1, CV_SHAPE_CUSTOM,
//                        new int[]{0, 0, 0, 1, 1, 1, 0, 0, 0});
//                IplConvKernel kernely = IplConvKernel.create(3, 3, 1, 1, CV_SHAPE_CUSTOM,
//                        new int[]{0, 1, 0, 0, 1, 0, 0, 1, 0});

//                cvErode(diff1, diff1, null,5);
//                cvDilate(diff1, diff1, null, 5);
//
//                cvErode(diff2, diff2, null,5);
//                cvDilate(diff2, diff2, null, 5);

//                cvSmooth(diff1, diff1, CV_GAUSSIAN, 5, 5, 2, 2);
//                cvSmooth(diff2, diff2, CV_GAUSSIAN, 5, 5, 2, 2);

                cvThreshold(diff1, diff1, 10, 255, CV_THRESH_BINARY);
                cvThreshold(diff2, diff2, 10, 255, CV_THRESH_BINARY);
                comdiff = Utilities.black(diff1);
                cvAnd(diff1, diff2, comdiff);
//                cvErode(comdiff, comdiff, null,1);
//                cvSmooth(comdiff, comdiff, CV_GAUSSIAN, 5, 5, 2, 2);
//                cvThreshold(comdiff, comdiff, 10, 255, CV_THRESH_BINARY);



                CvSeq contour = new CvSeq();
                CvMemStorage storage = CvMemStorage.create();
                scan = Utilities.black(comdiff);
                cvFindContours(comdiff, storage, contour, Loader.sizeof(CvContour.class), CV_RETR_EXTERNAL,

                        CV_CHAIN_APPROX_SIMPLE);// CV_CHAIN_APPROX_SIMPLE);
                for (; contour != null && !contour.isNull(); contour = contour.h_next()) {
//                    opencv_core.CvSeq approx = cvApproxPoly(contour, Loader.sizeof(opencv_core.CvContour.class), storage, CV_POLY_APPROX_DP,
//                            cvContourPerimeter(contour) * 0.001, 0);
//                    opencv_core.CvRect rec = cvBoundingRect(approx, 0);
                    cvDrawContours(scan, contour, CvScalar.WHITE, CvScalar.WHITE, 1, -1, 8);
                }
                out=Utilities.black(image2);
                cvCopy(image2,out,scan);
                cf.showImage(Utilities.conv.convert(out));
            }


            if (image3 != null) image3.release();
            image3 = Utilities.copy(image2);

            if (image2 != null) image2.release();
            image2 = Utilities.copy(image1);

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.exit(0);
    }

    public static void main(String[] args) {
        new Overseer();
    }
}
