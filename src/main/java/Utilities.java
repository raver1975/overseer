import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.bytedeco.javacv.OpenCVFrameConverter;

import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_imgproc.CV_RGB2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.cvCvtColor;

/**
 * Created by Paul on 4/17/2016.
 */
public class Utilities {

    public static OpenCVFrameConverter.ToIplImage conv = new OpenCVFrameConverter.ToIplImage();
    public static Java2DFrameConverter conv2d = new Java2DFrameConverter();


    public static IplImage copy(IplImage image) {
        if (image == null || image.isNull()) return null;
        IplImage copy = null;
        if (image.roi() != null)
            copy = IplImage.create(image.roi().width(), image.roi().height(), image.depth(), image.nChannels());
        else copy = IplImage.create(image.cvSize(), image.depth(), image.nChannels());
        cvCopy(image, copy);
        return copy;
    }

    public static IplImage diff(IplImage image1, IplImage image2) {
        IplImage diff = IplImage.create(image1.width(), image1.height(), IPL_DEPTH_8U, 3);
        cvAbsDiff(image1, image2, diff);
        return gray(diff);
    }


    public static IplImage gray(IplImage image) {
        if (image == null) return null;
        IplImage copy = null;
        if (image.roi() != null)
            copy = IplImage.create(image.roi().width(), image.roi().height(), IPL_DEPTH_8U, 1);
        else
            copy = IplImage.create(image.cvSize(), IPL_DEPTH_8U, 1);
        cvCvtColor(image, copy, CV_RGB2GRAY);
        return copy;
    }

    public static IplImage black(IplImage image) {
        IplImage temp = null;
        if (image.roi() != null)
            temp = IplImage.create(image.roi().width(), image.roi().height(), image.depth(), image.nChannels());

        else
            temp = IplImage.create(image.cvSize(), image.depth(), image.nChannels());
        cvZero(temp);
        return temp;
    }

    public static IplImage white(IplImage image) {
        IplImage temp = null;
        if (image.roi() != null)
            temp = IplImage.create(image.roi().width(), image.roi().height(), image.depth(), image.nChannels());

        else
            temp = IplImage.create(image.cvSize(), image.depth(), image.nChannels());
        cvZero(temp);
        cvNot(temp,null);
        return temp;
    }

}
